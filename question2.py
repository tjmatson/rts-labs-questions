def rotate_string(string, n):
	"""Return the input string rotated n spaces"""

	# Reduce multiple rotations to a single rotation
	n = n % len(string)
	if n == 0: # No rotation neccesary
		return string

	# Doesn't work with n = 0
	return string[len(string) - n:] + string[:-n]


if __name__ == '__main__':
	# Testing
	string = 'MyString'

	for i in range(0, len(string) * 2 + 1):
		print(rotate_string(string, i))

	print('-- Reversed')
	for i in range(0, len(string) + 1):
		print(rotate_string(string, -i))