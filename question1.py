def above_below(nums, target):
	"""Print the number of integers in the nums list that are above and below the target"""
	above = 0
	below = 0

	for num in nums:
		if num > target:
			above += 1
		elif num < target:
			below += 1

	print(f'above: {above}, below: {below}')


if __name__ == '__main__':
	# Testing
	inputs = [
		[[1, 5, 2, 1, 10], 6],
		[[1, 5, 2, 1, 10], -1],
		[[1, 5, 2, 1, 10], 12]
	]

	for args in inputs:
		above_below(*args)