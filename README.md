# RTS Labs Questions

### Question 1

> Print the number of integers in an array that are above the given input and the number that are below, e.g. for the array [1, 5, 2, 1, 10] with input 6, print “above: 1, below: 4”.

[Python solution](question1.py)

### Question 2

> Rotate the characters in a string by a given input and have the overflow appear at the beginning, e.g. “MyString” rotated by 2 is “ngMyStri”.

[Python solution](question2.py)

### Question 3

> If you could change 1 thing about your favorite framework/language/platform (pick one), what would it be?

What I would change about a language is being able to extend classes in C#, similiar to [extensions](https://kotlinlang.org/docs/reference/extensions.html) in Kotlin
or adding functions to prototypes in Javascript.

Adding extensions to a class allow for more concise, expressive code over writing functions that take objects
of that type. It also enables you to separate out context-specific methods rather than polluting the main class
with many different methods that may only be needed in specific contexts.
